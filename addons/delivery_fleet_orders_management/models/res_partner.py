# -*- coding: utf-8 -*-
import datetime

from odoo import api, fields, models, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    is_driver = fields.Boolean('Is driver', readonly=True)
    vehicle_id = fields.Many2one('fleet.vehicle', 'Vehicle')
    bi_directional = fields.Many2many('fleet.vehicle', 'bi_directional_rel')
    driver_vehicle_domain = fields.Many2many('fleet.vehicle', compute='compute_vehicle_count',)
    vehicle_count = fields.Integer(compute='compute_vehicle_count', store=True)
    delivery_order = fields.Many2many('stock.picking', domain=[('picking_type_id.code', '=', 'outgoing'), ('scheduled_date', '>=', datetime.datetime.now())],  string='Delivery Order')
    picking_count = fields.Integer(compute='compute_picking_count', store=True)

    def action_open_driver_pickings(self):
        return {
            'name': _("Delivery Pickings"),
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'domain': [('id', 'in', self.delivery_order.ids)],
        }

    @api.depends('delivery_order')
    def compute_picking_count(self):
        for rec in self:
            rec.picking_count = len(rec.delivery_order)

    def action_open_driver_vehicle(self):
        return {
            'name': _("Driver Related vehicles"),
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'fleet.vehicle',
            'domain': [('id', 'in', self.driver_vehicle_domain.ids)],
        }

    @api.depends('vehicle_id', 'bi_directional')
    def compute_vehicle_count(self):
        for rec in self:
            rec.vehicle_count = len(rec.vehicle_id) + len(rec.bi_directional)
            rec.driver_vehicle_domain = [[5, 0, 0]]
            rec.driver_vehicle_domain |= rec.bi_directional
            rec.driver_vehicle_domain |= rec.vehicle_id

