# -*- coding: utf-8 -*-
from odoo import  models, fields, api, _
import datetime


class FleetVehicle(models.Model):
    _inherit = 'fleet.vehicle'

    driver_ids = fields.One2many('res.partner', 'vehicle_id', string='Drivers')
    bi_driver_ids = fields.Many2many('res.partner', 'bi_directional_rel', string='Drivers')
    driver_vehicle_domain = fields.Many2many('res.partner', 'drivers_vehicle_rel', compute='compute_all_driver_ids', store=True)
    related_order_ids = fields.Many2many('stock.picking', string='related Orders', compute='compute_related_orders', store=True)
    today_related_order_count = fields.Integer(string='today related Orders', compute='compute_related_orders', store=True)
    future_related_order_count = fields.Integer(string='future related Orders', compute='compute_related_orders', store=True)
    all_related_order_count = fields.Integer(string='all related Orders', compute='compute_related_orders', store=True)

    @api.depends('driver_ids', 'bi_driver_ids')
    def compute_all_driver_ids(self):
        for rec in self:
            rec.driver_vehicle_domain = [[5, 0, 0]]
            rec.driver_vehicle_domain |= rec.driver_ids
            rec.driver_vehicle_domain |= rec.bi_driver_ids

    @api.depends('driver_vehicle_domain')
    def compute_related_orders(self):
        for rec in self:
            rec.related_order_ids = [[5, 0, 0]]
            for driver in rec.driver_vehicle_domain:
                rec.related_order_ids |= driver.delivery_order
            rec.today_related_order_count = len(rec.related_order_ids.filtered(lambda order: order.scheduled_date == datetime.datetime.now()))
            rec.future_related_order_count = len(rec.related_order_ids.filtered(lambda order: order.scheduled_date > datetime.datetime.now()))
            rec.all_related_order_count = rec.today_related_order_count + rec.future_related_order_count

    def open_related_future_orders(self):
        return {
            'name': _("Related Future Pickings"),
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'domain': [('id', 'in', self.related_order_ids.ids)],
        }